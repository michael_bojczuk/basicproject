﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCont : MonoBehaviour {

    public Transform target;

    [System.Serializable]
    public class camMovements
    {
        public Vector3 camOffset = new Vector3 (0,3, 0);
        public float distFromTarget = -8.0f;
        public float maxZoom = -2.0f;
        public float minZoom = -15.0f;
        public float zoomSmooth = 100.0f;
        public float xRotation = 20.0f;                               //rotations for target that we can update
        public float yRotation = -180.0f;
        public float camSmoothOrbit = 100.0f;
        public float lookSmooth = 100.0f;                           //smoothness of camera movement
    }

    public camMovements cam = new camMovements();

    Vector3 targetPos = Vector3.zero;
    Vector3 destination = Vector3.zero;
	// Use this for initialization
	void Start () {
        moveToPosition();
    }
	
	// Update is called once per frame
	void Update ()
    {
        ZoomInOnTarget();
    }

    void FixedUpdate()
    {
        moveToPosition();
        camInitPosition();
    }

    void moveToPosition()
    {
        targetPos = target.position + cam.camOffset;
        destination = Quaternion.Euler(cam.xRotation, cam.yRotation + target.eulerAngles.y, 0) * -Vector3.forward * cam.distFromTarget;
        destination += target.position;
        transform.position = destination;
    }
    //rotates between targets position and cameras position
    void camInitPosition()
    {
        Quaternion targetRotation = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, cam.lookSmooth * Time.deltaTime);
    }

    //scrollWheel zooming
    void ZoomInOnTarget()
    {
        cam.distFromTarget += Input.GetAxis("Mouse ScrollWheel") * cam.zoomSmooth * Time.deltaTime;

        if (cam.distFromTarget > cam.maxZoom)
        {
            cam.distFromTarget = cam.maxZoom;
        }
        if (cam.distFromTarget < cam.minZoom)
        {
            cam.distFromTarget = cam.minZoom;
        }
    }
}
