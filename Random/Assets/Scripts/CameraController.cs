﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;    //our target player

    [System.Serializable]
    public class PositionSettings
    {
        public Vector3 targetPosOffSet = new Vector3(0, 3.4f, 0); //Targets positon from origin
        public float lookSmooth = 100.0f;                           //smoothness of camera movement
        public float distFromTarget = -8.0f;
        public float zoomSmooth = 100.0f;
        public float maxZoom = -2.0f;
        public float minZoom = -15.0f;
        public bool smoothFollow = true;
        public float smooth = 0.05f;

        [HideInInspector]
        public float newDistande = -8.0f;  //set by zoom input
        [HideInInspector]
        public float adjustmentDistance = -8;
    }

    [System.Serializable]
    public class OrbitSettings
    {
        public float xRotation = -20.0f;                               //rotations for target that we can update
        public float yRotation = -180.0f;                              // same as above
        public float maxXRotation = 25.0f;                             //maximum it can rotate on vertical axis
        public float minXRotation = -20.0f;                            //min it can rotate on vertical axis
        public float maxYRotation = -90.0f;
        public float minYRotation = -270.0f;
        public float vOrbitSmooth = 20.0f;                            //veritcal and horizontal how fast we want the smoothing to take place
        public float hOrbitSmooth = 20.0f;
    }

    [System.Serializable]
    public class DebugSetting
    {
        public bool drawDesiredCollisionLines = true;
        public bool drawAdjustedCollisionLines = true;
    }

    public PositionSettings position = new PositionSettings();
    public OrbitSettings orbit = new OrbitSettings();
    public DebugSetting debug = new DebugSetting();
    public CollisionHandler collision = new CollisionHandler();

    Vector3 targetPos = Vector3.zero;
    Vector3 destination = Vector3.zero;
    Vector3 adjustedDestination = Vector3.zero;
    Vector3 camVel = Vector3.zero;
    CharacterController charController;
    float vOrbitInput, hOrbitInput, zoomInput, hOrbitSnapInput;

    //initialise
    void Start()
    {
        setCameraTarget(target);
        vOrbitInput = hOrbitInput = zoomInput = hOrbitSnapInput = 0;

        moveToTarget();

        collision.Initialize(Camera.main);
        collision.UpdateCameraClipPoints(transform.position, transform.rotation, ref collision.adjustedCameraClipPoints);
        collision.UpdateCameraClipPoints(destination, transform.rotation, ref collision.desiredCameraClipPoints);

        //turn off the cursor on play
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void setCameraTarget(Transform t)
    {
        target = t;
        if(target!= null)
        {
            if(target.GetComponent<CharacterController>())
            {
                charController = target.GetComponent<CharacterController>();
            }
            else
            {
                Debug.LogError("camera needs a target controller");
            }
        }
        else
        {
            Debug.LogError("camera needs a target");
        }
    }

    //get player input from mouse
    void GetInput()
    {
        //vOrbitInput = Input.GetAxisRaw("Mouse Y");
        //hOrbitInput = Input.GetAxisRaw("Mouse X");
        hOrbitInput = Input.GetAxis("OrbitHorizontal");
        vOrbitInput = Input.GetAxis("OrbitVertical");
        hOrbitSnapInput = Input.GetAxis("OrbitHorizontalSnap");
        zoomInput = Input.GetAxis("Mouse ScrollWheel");
    }

    void Update()
    {
        GetInput();
        ZoomInOnTarget();
    }

    //happens after update is called
    void FixedUpdate()
    {
        moveToTarget(); //making sure to reset position
        lookAtTarget(); //look at target based off rotations
        orbitTarget();  //player input orbit

        collision.UpdateCameraClipPoints(transform.position, transform.rotation, ref collision.adjustedCameraClipPoints);
        collision.UpdateCameraClipPoints(destination, transform.rotation, ref collision.desiredCameraClipPoints);

        //draw debug lines
        for(int i = 0; i < 5; i ++)
        {
            if(debug.drawDesiredCollisionLines)
            {
                Debug.DrawLine(targetPos, collision.desiredCameraClipPoints[i], Color.white);
            }
            if(debug.drawAdjustedCollisionLines)
            {
                Debug.DrawLine(targetPos, collision.adjustedCameraClipPoints[i], Color.green);
            }
        }

        //check colliding using raycats
        collision.checkColliding(targetPos);
        //setting new distance incase we collide
        position.adjustmentDistance = collision.getAdjustedDistanceWithRayFrom(targetPos);

    }

    //constantly update to check that camera is at target position
    void moveToTarget()
    {
        targetPos = target.position + position.targetPosOffSet;     //set initial position
        destination = Quaternion.Euler(orbit.xRotation, orbit.yRotation + target.eulerAngles.y, 0) * -Vector3.forward * position.distFromTarget;    //rotating around using vector3 to set intial
        destination += target.position;
        
        if(collision.colliding)
        {
            adjustedDestination = Quaternion.Euler(orbit.xRotation, orbit.yRotation + target.eulerAngles.y, 0) * Vector3.forward * position.adjustmentDistance;
            adjustedDestination += targetPos;

            if(position.smoothFollow)
            {
                //use smooth damp function
                transform.position = Vector3.SmoothDamp(transform.position, adjustedDestination, ref camVel, position.smooth);
            }
            else
            {
                transform.position = adjustedDestination;
            }
        }
        else
        {
            if (position.smoothFollow)
            {
                //use smooth damp function
                transform.position = Vector3.SmoothDamp(transform.position, destination, ref camVel, position.smooth);
            }
            else
            {
                transform.position = destination;
            }
        }
    }

    //looking at target during roations
    void lookAtTarget()
    {
        Quaternion targetRotation = Quaternion.LookRotation(targetPos - transform.position); //lookrotation is to compute the quaternion between two points which is our position and targets position
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, position.lookSmooth * Time.deltaTime); //interpolating the set of values returned 
    }

    void orbitTarget()
    {
        if(hOrbitSnapInput > 0)
        {
            orbit.yRotation = -180;
        }

        orbit.yRotation += -hOrbitInput * orbit.hOrbitSmooth * Time.deltaTime;
        orbit.xRotation += -vOrbitInput * orbit.vOrbitSmooth * Time.deltaTime; //getting the input ans smoothing it based on time

        if (orbit.xRotation > orbit.maxXRotation)
        {
            orbit.xRotation = orbit.maxXRotation;
        }
        if (orbit.xRotation < orbit.minXRotation)
        {
            orbit.xRotation = orbit.minXRotation;
        }

    }

    //scrollWheel zooming
    void ZoomInOnTarget()
    {
        position.distFromTarget += zoomInput * position.zoomSmooth * Time.deltaTime;

        if (position.distFromTarget > position.maxZoom)
        {
            position.distFromTarget = position.maxZoom;
        }
        if (position.distFromTarget < position.minZoom)
        {
            position.distFromTarget = position.minZoom;
        }
    }



    [System.Serializable]
    public class CollisionHandler
    {
        public LayerMask collisionLayer;
        [HideInInspector]
        public bool colliding = false;
        [HideInInspector]
        public Vector3[] adjustedCameraClipPoints;      //wherever the cameras position is
        [HideInInspector]
        public Vector3[] desiredCameraClipPoints;       //clip points where the collision would be if the camera was colliding 

        Camera camera;


        public void Initialize(Camera cam)
        {
            camera = cam;
            adjustedCameraClipPoints = new Vector3[5];
            desiredCameraClipPoints = new Vector3[5];

        }

        public void UpdateCameraClipPoints(Vector3 cameraPosition, Quaternion atRotation, ref Vector3[] intoArray)
        {
            if (!camera)
            {
                return;
            }

            //clear the contents of intoArray
            intoArray = new Vector3[5];

            float z = camera.nearClipPlane;
            float x = Mathf.Tan(camera.fieldOfView / 3.41f) * z;
            float y = x / camera.aspect;

            //top left
            intoArray[0] = (atRotation * new Vector3(-x, y, z) + cameraPosition);    //added and rotated the point relative to camera
            //top right
            intoArray[1] = (atRotation * new Vector3(x, y, z) + cameraPosition);    //added and rotated the point relative to camera
            //bottom left
            intoArray[2] = (atRotation * new Vector3(-x, -y, z) + cameraPosition);    //added and rotated the point relative to camera
            //bottom right
            intoArray[3] = (atRotation * new Vector3(x, -y, z) + cameraPosition);    //added and rotated the point relative to camera
            //cameras position
            intoArray[4] = (cameraPosition - camera.transform.forward);
        }

        bool CollisionDetectedAtClipPointss(Vector3[] clipPoints, Vector3 fromPosition)
        {
            for(int i = 0; i <clipPoints.Length; i++)
            {
                Ray ray = new Ray(fromPosition, clipPoints[i] - fromPosition);      //cast a ray at this position at this distance
                float distance = Vector3.Distance(clipPoints[i], fromPosition);     //returns true if hit when it runs into collision layer
                if(Physics.Raycast(ray,distance,collisionLayer))
                {
                    return true;
                }
            }
            return false;
        }

        //determines how far we move our camera forward when colliding
        public float getAdjustedDistanceWithRayFrom(Vector3 from)
        {
            float distance = -1;

            for(int i = 0; i < desiredCameraClipPoints.Length; i++)        //loop through desired clip array
            {
                Ray ray = new Ray(from, desiredCameraClipPoints[i] - from);
                RaycastHit hit;
                if(Physics.Raycast(ray, out hit))
                {
                    if(distance == -1)
                    {
                        distance = hit.distance;
                    }
                    else
                    {
                        if(hit.distance < distance)
                        {
                            distance = hit.distance;
                        }
                    }
                }
            }

            if(distance == -1)
            {
                return 0;
            }
            else
            {
                return distance;
            }
        }

        public void checkColliding(Vector3 targetPosition)
        {
            if (CollisionDetectedAtClipPointss(desiredCameraClipPoints, targetPosition))
            {
                colliding = true;
            }
            else
            {
                colliding = false;
            }
        }
    }


}

