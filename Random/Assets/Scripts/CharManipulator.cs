﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharManipulator : MonoBehaviour {

    [System.Serializable]
    public class MoveSettings
    {
        public float speed = 5.0f;
        public float sprintSpeed = 10.0f;
        public float jump = 5.0f;
        public float gravity = 9.6f;
        public float headCollisionGravity = 20f;
        public float turnVelocity = 5.0f;
    }
    //for raycasts
    [System.Serializable]
    public class raySets
    {
        public float distToAbove = 0.1f;
        public LayerMask Roof;
    }
    //instantiating the classes
    public MoveSettings move = new MoveSettings();
    public raySets ray = new raySets();

    //movement vector
    Vector3 moveDirection = Vector3.zero;
    //rotation
    Quaternion charRotation;

    CharacterController cont;

    // Use this for initialization
    void Start () {
        charRotation = transform.rotation;
        cont = GetComponent<CharacterController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        headCheck();
        turn();
    }
    //for movement
    void FixedUpdate()
    {
        movement();
    }
    //character movment
    void movement()
    {
        if (cont.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            if(Input.GetButton("Sprint"))
            {
                moveDirection *= move.sprintSpeed;
            }
            else
            {
                moveDirection *= move.speed;
            }
            
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = move.jump;
            }
        }
        moveDirection.y -= move.gravity * Time.deltaTime;
        cont.Move(moveDirection * Time.deltaTime);
    }
    //turning
    void turn()
    {
        charRotation *= Quaternion.AngleAxis(move.turnVelocity * Input.GetAxis("Mouse X") * Time.deltaTime, Vector3.up);
        transform.rotation = charRotation;
    }
    //raycast for above
    bool hitAbove()
    {
        return Physics.Raycast(transform.position, Vector3.up, ray.distToAbove, ray.Roof);
    }
    //manipulating velocity when hitting head
    void headCheck()
    {   
        if(hitAbove())
        {
            moveDirection.y -= move.headCollisionGravity * Time.deltaTime;
        }
    }
}
